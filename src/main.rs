#[macro_use]
extern crate diesel;

pub mod db;

use crate::db::Database;

fn main() {
    let database = Database::default();
    for event in database.show() {
        println!("{}", event.text);
    }
}
