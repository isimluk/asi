extern crate diesel;

pub mod models;
pub mod schema;

use self::models::{Event, NewEvent} ;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

pub struct Database {
    conn : SqliteConnection
}

impl Default for Database {
    fn default() -> Self {
        Database {
            conn : SqliteConnection::establish("asi.db").expect("Failed opening the db.")
        }
    }
}
impl Database {
    pub fn migrate(self) {}
    pub fn show(self) -> std::vec::Vec<Event> {
        use schema::events::dsl::*;

        let results = events.limit(5).load::<Event>(&self.conn).expect("Error loading events");
        return results;
    }

    pub fn store_event<'a>(self, text: &'a str) -> usize {
        use schema::events;

        let new_event = NewEvent { text: text };

        diesel::insert_into(events::table)
            .values(&new_event)
            .execute(&self.conn)
            .expect("Error saving new event")
    }
}
