#[derive(Queryable)]
pub struct Event {
    pub id: i64,
    pub text: String
}

use super::schema::events;

#[derive(Insertable)]
#[table_name="events"]
pub struct NewEvent<'a> {
    pub text: &'a str
}
